package yurasik.com.servisebindexample.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import yurasik.com.servisebindexample.ui.MainActivity;
import yurasik.com.servisebindexample.util.LogUtil;

/**
 * Created by Yurii on 6/20/17.
 */

public class ActivityBroadcastReceiver extends BroadcastReceiver {
    private MainActivity activity;

    @Override
    public void onReceive(Context context, Intent intent) {
        String message = intent.getStringExtra("data");
        LogUtil.i("onReceive: " + message);
        sendEvent(message);
    }

    public void sendEvent(String event) {
        if (activity!=null) {
            activity.setText(event);
        }
    }

    public MainActivity getActivity() {
        return activity;
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }
}

package yurasik.com.servisebindexample.util;

public class Const {
    public static final String COMMAND_RUN = "yurasik.com.servisebindexample.util.run";
    public static final String COMMAND_STOP = "yurasik.com.servisebindexample.util.stop";
    public static final String BROADCAST_ACTION_ADD_TO_HEAP = "yurasik.com.servisebindexample.util.addtoheap";
}

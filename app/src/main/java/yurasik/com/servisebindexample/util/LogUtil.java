package yurasik.com.servisebindexample.util;

import android.util.Log;

/**
 * Created by Yurii on 6/20/17.
 */

public class LogUtil {
    public static void i(String message) {
        Log.d("MyLog", message);
    }
}

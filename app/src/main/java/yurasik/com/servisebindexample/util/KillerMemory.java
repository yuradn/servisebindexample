package yurasik.com.servisebindexample.util;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Yurii on 6/20/17.
 */

public class KillerMemory {

    public static void start(ArrayList<Integer[]> bigArray) {
        synchronized (bigArray) {
            Integer[] garbageArray = new Integer[1000000];
            Random rnd = new Random();
            for (int i = 0; i < garbageArray.length; i++) {
                garbageArray[i] = rnd.nextInt(10000);
            }

            bigArray.add(garbageArray);
        }
    }
}

package yurasik.com.servisebindexample.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import yurasik.com.servisebindexample.util.Const;
import yurasik.com.servisebindexample.util.KillerMemory;
import yurasik.com.servisebindexample.util.LogUtil;

/**
 * Created by Yurii on 6/20/17.
 */

public class AlwaysService extends Service {

    protected ArrayList<Integer[]> marr;

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtil.i("OnCreate");
        marr = new ArrayList<>();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LogUtil.i("onStartCommand: " + intent + "\nstartId: " + startId+" service: " + this);
        switch (intent.getAction()) {
            case Const.COMMAND_RUN:
                KillerMemory.start(marr);
                Intent broadcastIntent = new Intent(Const.BROADCAST_ACTION_ADD_TO_HEAP);
                //intent.setAction(Const.BROADCAST_ACTION_ADD_TO_HEAP);
                broadcastIntent.putExtra("data","Notice me senpai! "+marr.size());
                sendBroadcast(broadcastIntent);
                LogUtil.i("Sending broadcast ***************");
                break;
            case Const.COMMAND_STOP:
                stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void run() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtil.i("Ondestroy my always service");
    }

}

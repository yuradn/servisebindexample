package yurasik.com.servisebindexample.ui;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

import yurasik.com.servisebindexample.R;
import yurasik.com.servisebindexample.receiver.ActivityBroadcastReceiver;
import yurasik.com.servisebindexample.service.AlwaysService;
import yurasik.com.servisebindexample.util.Const;
import yurasik.com.servisebindexample.util.KillerMemory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    protected Snackbar snackbar;
    protected ContentLoadingProgressBar progressBar;

    protected ArrayList<Integer[]> marr;
    private ActivityBroadcastReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        br = new ActivityBroadcastReceiver();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (snackbar!=null && snackbar.isShown()) return;
                snackbar = Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Action", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                LogUtil.i("snack click");
                                snackbar.dismiss();
                                snackbar = null;
                            }
                        });
                snackbar.show();*/

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        KillerMemory.start(marr);

                       setText("Killing");
                    }
                }).start();


            }
        });

        findViewById(R.id.btn_start).setOnClickListener(this);
        findViewById(R.id.btn_stop).setOnClickListener(this);
        progressBar = ContentLoadingProgressBar.class.cast(findViewById(R.id.progressbar));
        progressBar.hide();

        marr = new ArrayList<>();
    }

    public void setText(String event) {
        Log.d("Main", "Size: " + event);
        Toast.makeText(this, event, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.btn_start:
                progressBar.show();
                i = new Intent(v.getContext(), AlwaysService.class);
                i.setAction(Const.COMMAND_RUN);
                startService(i);
                break;
            case R.id.btn_stop:
                progressBar.hide();
                i = new Intent(v.getContext(), AlwaysService.class);
                i.setAction(Const.COMMAND_STOP);
                startService(i);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        br.setActivity(this);
        IntentFilter filter = new IntentFilter(Const.BROADCAST_ACTION_ADD_TO_HEAP);
        registerReceiver(br, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(br);
        br.setActivity(null);
    }
}
